/**
 * @file: script.js.
 */

(function ($) {

Drupal.behaviors.markdownToggle = {
  // Run when DOM is loaded.
  attach: function (context, settings) {

    $(".markdown-toggle").click(function(event) {
      // Don't follow the link.
      event.preventDefault();

      // Turn on / off.
      $(this).parent().find('.markdown-toggle-element').toggleClass('markdown-toggle-on');

    });

  }
};

})(jQuery);
